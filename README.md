NUCLEAR FISSION
===============

Nuclear Fission is a simple 2d game where the player has to shoot and dodge small atoms. Atoms split on impact and bounce around the stage. The goal is to not get hit and split enough atoms to get to the next stage. At the end of every stage a boss appears and must be defeated to win the level. To stay alive the player can collect health and upgrades for his gun. The game uses the Unity engine and a retro/cartoon artstyle. All assets (except the upgrades sprites for health/ammo and the laser shaders) are selfmade.

**Usage instruction: Make sure to download/clone the whole repository (The .exe alone won't suffice, the data folder is required).**